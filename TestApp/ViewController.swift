//
//  ViewController.swift
//  TestApp
//
//  Created by Anna on 03.05.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myTouchDownLbl: UILabel!
    
    @IBOutlet weak var mySwitch: UILabel!
    @IBOutlet weak var switchLable: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var stepperLabel: UILabel!
    
// MARK: - Switch
    
    @IBAction func actionSwitch(_ sender: UISwitch) {
        if sender.isOn {
            switchLable.text = "ON"
            imgView.isHidden = false
        } else {
            switchLable.text = "OFF"
            imgView.isHidden = true
        }
    }
    
// MARK: - Touch Down
    
    @IBAction func actionTouchDown(_ sender: UIButton) {
        
        myTouchDownLbl.text = "UP!"
        
    }
    
    @IBAction func actionTouchUpOutside(_ sender: UIButton) {
        myTouchDownLbl.text = "Down"
    }
    @IBAction func nextViewController(_ sender: UIButton) {
        
        let nextVC = UIViewController()
        self.navigationController?.pushViewController(nextVC, animated: true)
        
        nextVC.view.backgroundColor = .white
        
        let myBtn = UIBarButtonItem(title: "myBtn", style: .done, target: nil, action: nil)
        
        nextVC.navigationItem.rightBarButtonItem = myBtn
        
    }
    
// MARK: - Slider Change
    
    @IBAction func actionSliderChange(_ sender: UISlider) {
        
        sliderLabel.text = "Value: \(sender.value)"
        
    }
    
// MARK: - Stepper Change
    
    @IBAction func stepperChange(_ sender: UIStepper) {
        
        stepperLabel.text = "Value: \(sender.value)"
    }
    
    
// MARK: - ViewController
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        imgView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


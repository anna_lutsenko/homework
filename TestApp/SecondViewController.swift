//
//  SecondViewController.swift
//  TestApp
//
//  Created by Anna on 03.05.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }

}
